
void percentage(int *rolls, int results[], double percents[])
//Percentage of each of the sums
{
	int index_i;
	double dbl_rolls, percent;
	dbl_rolls = *rolls;

	for (index_i = 2; index_i <= 12; index_i++) { // Change the percent array to now hold the resulting percentage of the sums
		percent = (results[index_i] / dbl_rolls * 100);
		percents[index_i] = percent;
	}
}
