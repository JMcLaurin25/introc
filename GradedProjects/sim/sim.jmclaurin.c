
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "percent.h"
#include "roll_prob.h"

//Preprocessor definitions
#define POSSIBLE_SUMS 12
#define MAX_SUMS 100

int main(int argc, char *argv[])
//int main(void)
{
	int number_rolls = 100, multiplier = 1, roll1 = 0, roll2 = 0, sum = 0, remainder, whole;
	int results_array[MAX_SUMS] = {0};
	double difference, percent_array[MAX_SUMS] = {0.0};
	double probabilities[13] = {0.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 5.0, 4.0, 3.0, 2.0, 1.0};
	int index_i, index_j;

	switch (argc) { //Argument handling
		case 1:
			//Use default values
			break;
		case 2:
			if (strtod(argv[1], '\0')) {
				number_rolls = strtod(argv[1], '\0');
				printf("Num Rolls%d\n", number_rolls);
				if (number_rolls > 100000) { // Limit max entry number of rolls
					printf("\n\tWarning: Please enter a smaller number of rolls.\n\n");
					return(1);
				} else if (number_rolls <= 0) {
					printf("\n\tWarning: Please enter a number greater than zero.\n\n");
					return(1);
				}
				multiplier = 1;
			} else { //Not a numerical value
				printf("\n\tWarning: Provide numerical value greater than zero for roll count.\n\n");
				return(1);
			}
			break;
		case 3:
			if (strtod(argv[1], '\0') && strtod(argv[2], '\0')) {
				number_rolls = strtod(argv[1], '\0');
				multiplier = strtod(argv[2], '\0');

				if (multiplier > number_rolls) { //Notify that multiplier is larger than the roll number
					printf("\n\tWarning: Multiplier greater than roll number!\n\n");
				} else if (multiplier <= 0) {
					printf("\n\tWarning: Multiplier needs to be a positive integer!\n\n");
					return(1);
				}

			} else if (!strtod(argv[1], '\0')){ //Not a number
				printf("\n\tWarning: Provide numerical value for roll count.\n\n");
				return(1);
			} else if (!strtod(argv[2], '\0')) { //Not a number
				printf("\n\tWarning: Provide numerical value for multiplier.\n\n");
				return(1);
			} else {
				printf("\n\tWarning: Provide numerical value.\n\n");
				return(1);
			}
			break;
		default:
			printf("\n\tWarning: Incorrect number of arguments!\n\n");
			return(1);
	}

	for (index_i = 0; index_i < number_rolls; index_i++) { //Generate random rolls and count in the results_array[]
		roll(&roll1);
		roll(&roll2);
		sum = roll1 + roll2;
		results_array[sum]++;
	}

	//Generate the calculated results and percentages.
	percentage(&number_rolls, results_array, percent_array);
	probability(probabilities);

	// Results output
	printf("> %d rolls of the dice.\n", number_rolls);
	printf("%4s | %4s | %6s | %4s | %6s | Histogram Multiplier: %d\n", "Sum", "Count", "%", "Chance", "Diff", multiplier);
	for (index_i = 2; index_i <= POSSIBLE_SUMS; index_i++) {
		difference = percent_array[index_i] - probabilities[index_i];
		printf("%4d | %5d | %6.2f | %6.2f | %6.2f | ", index_i, results_array[index_i], percent_array[index_i], probabilities[index_i], difference);

		remainder = results_array[index_i] % multiplier;
		whole = results_array[index_i] / multiplier;
		for (index_j = 0; index_j < whole; index_j++)
			printf("*");
		printf(" %d\n", remainder);
	}
	return (0);
}
