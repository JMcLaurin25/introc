
int roll(int *result)//Generates the random roll value from 1 - 6
{
	*result = 1 + rand() % 6;
}

void probability(double probabilities[])
{
	int index_i, outcomes = 36; //36 possible permutations
	double prob;

	for (index_i = 2; index_i <= 12; index_i++) {//Take the defined array and change to the calculated percentage
		prob = probabilities[index_i] / outcomes * 100;
		probabilities[index_i] = prob;
	}
}
