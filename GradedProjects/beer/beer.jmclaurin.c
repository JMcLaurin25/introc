/*beer.james.mclaurin.c*/
#include <stdio.h>
#include <stdlib.h>
#include "verseout.h"
#include "verseformat.h"

//Preprocessor definitions
#define MAX 50
#define WORD_LENGTH 25

int main(int argc, char *argv[])
{
    //oneDigit 2D array stores charArry for non-numeric: 1-9
    char numbers[MAX][WORD_LENGTH] = {"No more", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"};
    //oneDigit 2D array stores charArry for non-numeric: 10-90
    char compoundNumbers[MAX][WORD_LENGTH] = {"", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety", ""};

    char combined[WORD_LENGTH] = "", combined2[WORD_LENGTH] = "";
    int start_value, is_alpha = 0, index1;

	//Argc switch
	switch(argc) {
		case 1:
			start_value = 99;
			break;

		case 2:
			if (!strcmp(argv[1], "-n")) { //'-n' option prints the numerical output
				is_alpha = 1;
				start_value = 99;
			} else {
				start_value = strtod(argv[1], '\0');
			}

			if (start_value > 99 || start_value <= 0) {
				printf("Invalid arguments!\n");
				return(1);
			}
			break;

		case 3:
			//Read arguments. argv[1] decides numerical or alpha. argv[2] is the count
			if (!strcmp(argv[1], "-n")) {
				is_alpha = 1;
				start_value = strtod(argv[2], '\0');
			} else {
				printf("Invalid arguments, or use. (-n 75).\n");
				return(1);
			}

			if (start_value == 0) { //Checking for a valid entry
				printf("Provide a valid numerical entry.\n");
				return(1);
			} else if (start_value > 99 || start_value <= 0) {
				printf("Number out of bounds! Provide a value below 100 and greater than 0.\n");
				return(1);
			}
			break;

		default:
			printf("Invalid arguments!\n");
			break;
	}
	
	switch(is_alpha) { //To print either numerics or alphabetic values.
		case 0:
			for (index1 = start_value; index1 > 0; index1--) {
				if (index1 >= 0 && index1 < 20) { // 0 - 20 
				    verseOutAlpha(&index1, numbers[index1], numbers[index1 - 1]);
				} else if (index1 == 20) { //Since 20 and 19 will span two different char arrays.
				    verseOutAlpha(&index1, compoundNumbers[index1 / 10], numbers[index1-1]);
				} else {
				    if (index1 % 10 == 0) { // For tens... 30, 40, 50, etc. 
				        combineNums(combined, compoundNumbers[(index1 / 10) - 1], numbers[9]);
				        verseOutAlpha(&index1, compoundNumbers[index1 / 10], combined);
				    } else if (index1 % 10 == 1) { // To manage compound values and a non-compound("Thirty-One" & "Thirty"
				        combineNums(combined, compoundNumbers[index1 / 10], numbers[index1 % 10]);
				        verseOutAlpha(&index1, combined, compoundNumbers[(index1 - 1) / 10]);
				    } else {
				        combineNums(combined, compoundNumbers[index1 / 10], numbers[index1 % 10]);
				        combineNums(combined2, compoundNumbers[index1 / 10], numbers[(index1 % 10) - 1]);
				        verseOutAlpha(&index1, combined, combined2);
				    }
				}
				printf("\n");   
			}
			break;

		case 1: //Default 99 values
			for (index1 = start_value; index1 > 0; index1--) {
				verseOutNumeric(&index1);
				printf("\n");   
			}
			break;

		default:
			break;
	}

    printf("Bottle count began at: %d\n", start_value);
	return(0);
}
