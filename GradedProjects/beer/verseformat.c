#include <string.h>

void plurality(int *bottle, char *phrase)
{
    if (*bottle == 1) //Plurality of 'bottle' phrase of the verse.
        strncpy(phrase, "bottle", 7);
    else
        strncpy(phrase, "bottles", 8);
}

void combineNums(char *output, char *tens, char *ones) //Generates the compound number. ei: Thirty-One
{
    strncpy(output, "", 1);
    strncat(output, tens, strlen(tens));
    strncat(output, "-", 1);
    strncat(output, ones, strlen(ones));
}
