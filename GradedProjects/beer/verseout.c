#include <stdio.h>

#define WORD_LENGTH 25

//Function prints the current verse with given value
void verseOutAlpha(int *number, char numWord1[], char numWord2[])
{
    int number2 = *number - 1;
    char singlePlural[WORD_LENGTH], singlePlural2[WORD_LENGTH];

    //These determine the plurality of the word "bottle(s)"
    plurality(number, singlePlural);
    plurality(&number2, singlePlural2);
	
    printf("%s %s of beer on the wall!\n%s %s of beer!\nTake one down\nAnd pass it around\n%s %s of beer on the wall!\n", numWord1, singlePlural, numWord1 , singlePlural, numWord2, singlePlural2);
}

void verseOutNumeric(int *number) // Output when '-n' is provided
{
    int number2 = *number - 1;
    char singlePlural[WORD_LENGTH], singlePlural2[WORD_LENGTH];

    //These determine the plurality of the word "bottle(s)"
    plurality(number, singlePlural);
    plurality(&number2, singlePlural2);


	if (number2 == 0) { //Convert 0 bottles to 'No more'
		printf("%d %s of beer on the wall!\n%d %s of beer!\nTake one down\nAnd pass it around\n%s %s of beer on the wall!\n", *number, singlePlural, *number , singlePlural, "No more", singlePlural2);
	} else {
	    printf("%d %s of beer on the wall!\n%d %s of beer!\nTake one down\nAnd pass it around\n%d %s of beer on the wall!\n", *number, singlePlural, *number , singlePlural, number2, singlePlural2);
	}
}

