/*three_Twelve.c*/
#include <stdio.h>

int consecutiveSum(int value);//Prototype

int main(void)
{
    int lowNum = 1, highNum = 1, maxLim = 10000, total = 0;

    while (total != maxLim) {
        total = consecutiveSum(highNum);
        while (total > maxLim) {
            total -= consecutiveSum(lowNum);
            lowNum++;
        }
        highNum++;
    }
    printf("%d-%d = %d\n", lowNum, highNum, maxLim);

}

int consecutiveSum(int value)
{
    int sum = 0;
    sum = (value * (value + 1)) / 2;
    return sum;
}
