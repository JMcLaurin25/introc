/*three_Nine.c*/
#include <stdio.h>
#include <math.h>// Practice using header files

main()
{
    int num = 20;
    printf("%s %8s %8s\n", "INT", "SQUARED", "CUBED");
    for (num; num <= 60; num += 2)
    {
        int numSq, numCb;
        numSq = pow(num, 2);
        numCb = pow(num, 3);

        printf("%3d %8d %8d\n", num, numSq, numCb);
    }
}
