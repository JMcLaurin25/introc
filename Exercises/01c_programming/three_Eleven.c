/*three_Eleven.c*/
#include <stdio.h>

main()
{
    int i = 1, upperLim = 10, result = 1;
    while (i <= upperLim)
    {
        result = result * i;
        i++;
    }
    printf("%d factorial is: %d\n", upperLim, result);
}
