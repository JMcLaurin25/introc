/*five_chars.c*/
#include <stdio.h>
#include <string.h>

int main(void)
{
	int count = 0, lines = 0, c;
    
    //c = getchar();
	while((c = getchar()) != EOF) {
		count++;
        putchar(c);
        if( c == '\n' ) {
            lines++;
        }
    }


	printf("%d characters \n", count);
    printf("%d lines \n", lines);
}
