#include <stdio.h>

#define DIGITS 1

int check (char line[])
{
	int i = 0;

	// Checks for null character. And non-digits.

	for (; line[i] != '\0'; i++) {
		if (line[i] < '0' || line[i] > '9') {
			return(! DIGITS);
		}
	}
	return DIGITS;
}
