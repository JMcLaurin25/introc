/*five_copy2.c*/
//Copy standard input to standard output using the EOF defined element
#include <stdio.h>

int main(void)
{
	int c;
    c = getchar();
	while ( c != EOF ) {
		putchar(c);
        c = getchar();
    }
}
