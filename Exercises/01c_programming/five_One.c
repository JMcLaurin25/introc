/*five_One.c*/
#include <stdio.h>

int main(void)
{
	int ch;
	while (( ch = getchar()) != EOF)
		if (ch < '0' || ch > '9') {
			putchar(ch);
		}

}
