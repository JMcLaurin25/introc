/*six_Two.c

- Write a function that receives two 'int's, a base and a power and returns a 'long', the base raised to the power. A 'long int' is printed with %ld.
*/

#include <stdio.h>

long power(int base, int exponent);

int main(void)
{
	long int ans;
	int a=3, b=9;

	ans = power(a, b);
	printf("%d raised to the %d power is %ld\n", a, b, ans);
}

long power(int base, int exponent)
{
	int i = 1;
	long result = base;

	while (i < exponent) {
		result *= base;
		i++;
	}

	return result;
}
