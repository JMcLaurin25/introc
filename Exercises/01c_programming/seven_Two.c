/* seven_Two.c

- Write the function 'unset' which sets the nth bit of a variable to zero.
- The prototype is:
	unsigned int unset(unsigned int variable, int bit_number);

*/

#include <stdio.h>
#include <string.h>

#define MAX 40

unsigned int unset(unsigned int variable, int bit_number);
void printBin(unsigned int x);

int main(void)
{
	char input[MAX];
	unsigned int mask = ~0, bitUnset = 1;
	int num, i;

	printf("Which bit do you want to set? ");

	fgets(input, MAX, stdin);
	input[strlen(input + 1)] = '\0';

	num = atoi(input);

	//printf("mask: %d\n", mask);
	printBin(mask);
	
	
	bitUnset = unset(mask,num);

	printf("%d\n", (mask ^ num));
	printBin(bitUnset ^ num);
}

unsigned int unset(unsigned int mask, int num)
{
	register int i;

	
	for(i = 1; i < num; i++) {
		mask <<= 1;
		
		//printf("%d\n", bitSet);
	}
	return(bitUnset);
}

void printBin(unsigned int x)
{
	while (x) {
		if (x & 1)
			printf("1");
		else
			printf("0");
		x >>= 1;
	}
	printf("\n");
}
