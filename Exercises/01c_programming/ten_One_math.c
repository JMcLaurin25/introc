#include <stdio.h>

int atoi (char line[])
{
	int i = 0, n = 0;

	while (line[i] >= '0' && line[i] <= '9') {
		n = 10 * n + line[i++] - '0';
	}

	return (n);
}

double average (int nums[], int amt)
{
	int i, sum = 0;

	for (i = 0; i < amt; i++) {
		sum += nums[i];
	}
	return((double) sum / amt);
}
