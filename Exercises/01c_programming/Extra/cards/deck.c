#include "deck.h"

#include <stdlib.h>

struct deck *make_deck(void)
{
	int suit, rank;
	struct deck *pile;
	pile = malloc(sizeof(struct deck));
	if(pile == NULL) {
		return NULL;
	}

	for (suit = 0; suit < 4; ++suit) {
		for (rank = 2; rank < 15; ++rank) {
		pile->cards[suit * 13 + rank - 2].rank = rank;
		pile->cards[suit * 13 + rank - 2].suit = suit;
		}
	}
	pile->cards_used = 0;

	return pile;
}

void free_deck(struct deck *pile)
{
	free(pile);
}


void print_deck(struct deck *pile)
{
	int n;
	for(n = pile->cards_used; n < 52; ++n) {
		print_card(pile->cards[n]);
	}
}
