
#ifndef DECK_H
#define DECK_H

#include "card.h"

extern const char * deck_color;

/* Array of cards
 * Only careds from cards_used to 
 * index 51 are valid cards in
 * the deck.
 */

struct deck {
	struct card cards[52];
	int cards_used;
};

void free_deck(struct deck *);

void print_deck(struct deck*);

struct deck *make_deck(void);

#endif
