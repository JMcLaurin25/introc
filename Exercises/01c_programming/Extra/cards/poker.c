#include <stdio.h>

#include "card.h"
#include "deck.h"

int main(void)
{
	struct deck *bicycle;
	bicycle = make_deck();
	if (bicycle == NULL) {
		return 1;
	}

	print_deck(bicycle);
	free_deck(bicycle);
}
