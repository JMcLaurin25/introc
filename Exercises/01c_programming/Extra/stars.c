/*
Write a program that prompts the user to enter a positive integer, N, then outputs N lines each with N stars.

> anantha
Enter the number of lines: 5
*
**
***
****
*****
> anantha
Enter the number of lines: 1
*
> anantha
Enter the number of lines: 4
*
**
***
****
*/

#include <stdio.h>
#include <string.h>

#define MAX 100

int main(void)
{
    char input[MAX];
    int num, i, j;

    printf("Enter the number of lines: ");
    fgets(input, MAX, stdin);
    input[strlen(input + 1)] = '\0';

    num = atoi(input);

    for (j = 0; j <= num; j++) {
        for (i = 0; i <+ j; i++) {
            printf("*");
        }
    printf("\n");
    }
}
