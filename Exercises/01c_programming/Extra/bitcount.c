/*bitcount.c*/

//Counts the bits on a machine

#include <stdio.h>

int wordlength(void);

int main(void)
{
	int size;
	size = wordlength();

	printf("%d: bit(s) in an int\n", size);
}


int wordlength(void)
{
	unsigned int x = ~0; //Compliment of '0' (All 1's)
	int count = 0;

	while (x != 0) {
		count++;
		x >>= 1; //Shift right
	}

	return count; //returns count of bits in an unsigned int.
}

