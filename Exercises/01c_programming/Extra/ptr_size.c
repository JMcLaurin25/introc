#include <stdio.h>

int main(void)
{
	int *empty;
	int year = 1881;

	empty = &year;
	
	printf("empty    : %p\n", (void*)empty);
	printf("empty + 1: %p\n", (void*)(empty + 1));
	printf("empty + 2: %p\n", (void*)(empty + 2));

}
