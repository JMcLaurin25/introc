#include <stdio.h>

#define MAX 10

int main(void)
{
	int x[MAX] = {12, 14, 16, 18, 20, 22, 24, 26, 28};
	int *px, sum = 0;

	for ( px = x + MAX - 1; px >= x ; px--) {
		sum += *px;
		printf("- %d\n", sum);
	}

	printf("Sum: %d\n", sum);
}
