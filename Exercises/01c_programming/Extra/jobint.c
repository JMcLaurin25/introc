/*
The following problem is a well-known job interview filter question, often eliminating over 99.5% of candidates.
Write a program that takes an positive integer as input. The program should then print out the numbers from 1 to the input. But, for multiples of 3, print "Fizz" instead of the number, and for multiples of 5, print "Buzz". For numbers which are multiples of both 3 and 5, print "FizzBuzz".

> fizzbuzz
Enter a number to FizzBuzz: 10
1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
*/

#include <stdio.h>
#include <string.h>

#define MAX 100

int modThree(int value);
int modFive(int value);

int main(void)
{
    char input[MAX];
    int num, i;
    
    printf("Enter a number to FizzBuzz: ");
    fgets(input, MAX, stdin);
    input[strlen(input + 1)] = '\0';

    num = atoi(input);

    for (i = 1; i <= num; i ++) {
        if (modThree(i) == 0)
            printf("Fizz\n");
        else if (modFive(i) == 0)
            printf("Buzz\n");
        else
            printf("%d\n", i);
    }

}

int modThree(int value)
{
    return (value % 3);
}

int modFive(int value)
{
    return (value % 5);
}
