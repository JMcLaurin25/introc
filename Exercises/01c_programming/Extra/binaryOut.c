/*binaryOut.c*/
#include <stdio.h>
#include <stdlib.h>

#define MAX 100

void reverse(char [], char []);
int readIn(char []);

int main(void)
{
	char input[MAX], binary[MAX], revBin[MAX];
	int x, origX, mask = 1, i = 0;
	
	x = readIn(input);
	origX = x;

	while (x != 0) {
		if ((x & mask) == 1)
			binary[i] = '1';
		else 
			binary[i] = '0';
		x >>= 1;
		i++;
	}
	binary[i] = '\0';

	reverse(binary, revBin);

	printf("%d: %s\n", origX, revBin);
}

void reverse(char list[], char reverse[])
{
	int i = 0, length;
	
	while (list[i] != '\0') {
		i++;
	}

	length = i - 1;
	i = 0;
	
	reverse[length] = '\0';
	while ((length) >= 0) {
		reverse[i] = list[length];
		i++;
		length--;
	}
	reverse[i] = '\0';
}

int readIn(char input[])
{
	int x, i = 0;
	fgets(input, MAX, stdin);
	while (input[i] != '\n')
		i++;

	input[i] = '\0';
	
	x = atoi(input);
	return (x);
}


