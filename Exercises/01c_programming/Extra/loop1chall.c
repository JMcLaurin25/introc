/*loop1.c*/
/*
Write a program that inputs a character, and then a number, and then prints that many of the character.

> multiprinter
Enter a character: *
Enter a number: 50
**************************************************
> multiprinter
Enter a character: jj
Enter a number: 10
jjjjjjjjjjjjjjjjjjjj

> multiprinter
Enter a character: .
Enter a number: 3
...

Challenge: If the user enters a string instead of a character, print as many copies of the string as possible without printing more total characters than the number entered.

> multiprinter
Enter a character: trololo
Enter a number: 23
trololotrololotrololo
*/
#include <stdio.h>
#include <string.h>

#define MAX 100

int main(void)
{
    char userCh[MAX], count[MAX];
	int num, i = 0;
	
    printf("> multiprinter\n");
    printf("Enter a character: ");
    fgets(userCh, MAX, stdin);
    userCh[strlen(userCh + 1)] = '\0';

    printf("Enter a number: ");
    fgets(count, MAX, stdin);
    count[strlen(count + 1)] = '\0';


	num = atoi(count);
    
    while ( i < num ) {
        i++;
        if (strlen(userCh) * i >= num)
            break;
        printf("%s", userCh);
    }
    printf("\n");

}

