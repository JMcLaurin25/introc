#include <stdio.h>
#include <string.h>

#define MAX 100

void reverse(char []);
void changecase(char []);
int mystrcmp(char [], char[]);

int main(void)
{
	char input[MAX], comp1[MAX], comp2[MAX];

	fgets(input, MAX, stdin);
	input[strlen(input) - 1] = '\0';

	reverse(input);
	printf("Reversed: %s\n", input);

	changecase(input);
	printf("ChangeCase: %s\n", input);

	fgets(comp1, MAX, stdin);
	comp1[strlen(comp1) - 1] = '\0';

	fgets(comp2, MAX, stdin);
	comp2[strlen(comp2) - 1] = '\0';

	if (mystrcmp(comp1, comp2) == 0) {
		printf("%s and %s are equal\n", comp1, comp2);
	} else {
		printf("%s and %s are NOT equal\n", comp1, comp2);
	}

}

void reverse(char line[])
{
	char temp[MAX];
	int i, j = 0;
	
	i = strlen(line);
	temp[i + 1] = '\0';

	for (; i >= 1; i--) {
		temp[i-1] = line[j];
		j++;
	}

	for(i=0; i < strlen(line);i++)
		line[i] = temp[i];
		
}

void changecase(char line[])
{
	int i, inv;
	inv = 'a' - 'A';
	
	for (i = 0; i < strlen(line); i++) {
		if (line[i] >= 'a' && line[i] <= 'z')
			line[i] = line[i] - inv;
		else if (line[i] >= 'A' && line[i] <= 'Z')
			line[i] = line[i] + inv;
	}
}

int mystrcmp(char line1[], char line2[])
{
	int i = 0;

	while (line1[i] == line2[i]) {
		if (line1[i++] == '\0') {
			return(0);
		} 
	return(line1[i] - line2[i]);
	}
}
