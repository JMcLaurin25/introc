#include <stdio.h>

#define MAX 100

void stringcat(char line[], char target[]);

int main(void)
{
	char left[MAX] = "this";
	char right[MAX] = "that";
	stringcat(left, right);
	printf("%s %s\n", left, right);
}

void stringcat(char line[], char target[])
{
	int i = 0, j = 0;

	while(target[i] != '\0')
		i++;

	while( line[j] != '\0') {
		target[i++] = line[j++];
	}
	
}
