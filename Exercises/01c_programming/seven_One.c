/* seven_One.c

- Write the function set which sets bit 'n' of a variable 'x'.
- The prototype is:
	unsigned int set(unsigned int variable, int bit_number);
- Assume bit number zero is the rightmost bit of the integer.

*/

#include <stdio.h>
#include <string.h>

#define MAX 40

int main(void)
{
	char input[MAX];
	unsigned int mask = ~0, bitSet = 1;
	int num, i;

	printf("Which bit do you want to set? ");

	fgets(input, MAX, stdin);
	input[strlen(input + 1)] = '\0';

	num = atoi(input);

	//printf("mask: %d\n", mask);

	for(i = 0; i < num; i++) {
		bitSet <<= 1;
		//printf("%d\n", bitSet);
	}

	printf("%d\n", (bitSet & mask));
}
