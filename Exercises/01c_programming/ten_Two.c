/*ten_Two.c*/
#include <stdio.h>

int array[1000];

void put(int value, int position);
int get(int position);

int main(void)
{
	int x;
	put(10, 5);	//array[5] = 10;

	x = get(5); //x = array[5];
	printf("%d\n", x);
}

void put(int value, int position)
{
	extern int array[1000];
	array[position] = value;
}

int get(int position)
{
	extern int array[1000];
	return (array[position]);
}
