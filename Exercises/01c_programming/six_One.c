#include <stdio.h>
#include <string.h>

#define MAX 100
#define ALPHA 1

int alphacheck(char string[]);

int main(void)
{
	char line[MAX];
	while (1) {
		fgets(line, MAX, stdin); 
		line[strlen(line) - 1] = '\0';

		if (strcmp(line, "quit") == 0) {
			break;
		}

		if (alphacheck(line) == ALPHA) {
			printf("%s contains only alphabetic characters.\n", line);
		} else {
			printf("%s does NOT contain only alphabetic characters.\n", line);
		}
	}
}

int alphacheck(char string[])
{
	int i = 0, isAlpha = 0;

	while (string[i] != '\0') {
		//printf("%c <-Character\n", string[i]);
		if ((string[i] >= 'a' && string[i] <= 'z') || (string[i] >= 'A' && string[i] <= 'Z')) {
			isAlpha += 0;
		} else {
			isAlpha++;
		}
		i++;
	}

	if (isAlpha != 0) {
		return !ALPHA;
	} else {
		return ALPHA;
	}
}
