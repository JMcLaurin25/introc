/*sizes.c*/
#include <stdio.h>

int main(void)
{
    printf("%20s: %d\n", "Size of int", sizeof(int));
    printf("%20s: %d\n", "Size of char", sizeof(char));
    printf("%20s: %d\n", "Size of double", sizeof(double));
    printf("%20s: %d\n", "Size of float", sizeof(float));

    printf("\n%20s: %d\n", "Size of char*", sizeof(char*));
    printf("%20s: %d\n", "Size of int*", sizeof(int*));
    printf("%20s: %d\n", "Size of float*", sizeof(float*));
    printf("%20s: %d\n", "Size of void*", sizeof(void*));
    printf("%20s: %d\n", "Size of void(*)( )", sizeof(void(*)()));
    return 0;
}

/*
char
short
int
long int
long long
float
double
long double

*/
