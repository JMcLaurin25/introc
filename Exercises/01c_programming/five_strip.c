/*five_strip.c*/
//Removes the non-digit numbers. Makes the string digit-only
#include <stdio.h>

int main(void)
{
	int c;

	while(( c = getchar()) != EOF)
		if( c >= '0' && c <= '9')
			putchar(c);
}
