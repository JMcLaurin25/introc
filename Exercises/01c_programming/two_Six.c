/*two_Six.c*/
#include <stdio.h>
#include <math.h>

/*
m = p * (j / (1 - ((1 + j) ** -n))
m = Monthly Payment
p = Loan Amount
I = Interest Rate
j = i / (12 * 100)
n = Number of months over which loan is amortized
*/

double payments(double p, double i, double n)
{
    double m, j;

    j = i / (1200);
    m = p * (j / (1 - (pow((1 + j), (-n)))));
    /*printf("%.2f\n", m);*/
    return(m);
}

main()
{
    double p, i, n, bill;
    p = 200000;
    i = 7.5;
    n = 30 * 12;
    printf("\n%5s = %s\n", "M", "Monthly payment");
    printf("%5s = %s\n", "P", "Loan amount");
    printf("%5s = %s\n", "I", "Interest Rate");
    printf("%5s = %s\n", "J", "I / (12 x 100)");
    printf("%5s = %s\n", "N", "Number of loan months");


    printf("\n%5s = %s%.2f", "P", "$", p);
    printf("%5s = %.1f%s", "I", i, "%");
    printf("%5s = %.0f\n", "N", n);

    payments(p,i,n);
    bill = payments(p, i, n);
    printf("\n%5s = $%.2f\n", "M", bill);
    
}


