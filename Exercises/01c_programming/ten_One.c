/*six_extended.c*/

/* 
- Write a program prompting the user to enter a number.
- If a valid number is entered, it is placed into an array of integers.
- Repeat the process until the user enters the word 'quit'.
- The program prints the average of the numbers and then terminates.
*/

#include <stdio.h>
#include <string.h>

#define MAX 100
#define DIGITS 1
#define LIMIT 100

//Prototypes
void getaline(char line[]);
int strcompare (char [], char[]);
int check (char line[]);
int atoi (char line[]);
double average (int nums[], int amt);

//main
int main(void)
{
	char line[MAX];
	int numbers[LIMIT], i = 0;
	double answer;

	while (1) {
		printf("Enter a number (or 'quit'): ");
		getaline(line);

		if(strcompare(line, "quit") == 0)
			break;

		if(check(line) == DIGITS){
			numbers[i++] = atoi(line);
			if(i == LIMIT) {
				printf("Array is full!\n");
				break;
			}
		} else {
			printf("%s is not all digits.\n", line);
		}
	}
	
	answer = average(numbers, i);
	printf("The average is: %.2f\n", answer);
}




