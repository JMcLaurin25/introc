/*six_extended.c*/

/* 
- Write a program prompting the user to enter a number.
- If a valid number is entered, it is placed into an array of integers.
- Repeat the process until the user enters the word 'quit'.
- The program prints the average of the numbers and then terminates.
*/

#include <stdio.h>
#include <string.h>

#define MAX 100
#define DIGITS 1
#define LIMIT 100

//Prototypes
void getaline(char line[]);
int strcompare (char [], char[]);
int check (char line[]);
int atoi (char line[]);
double average (int nums[], int amt);

//main
int main(void)
{
	char line[MAX];
	int numbers[LIMIT], i = 0;
	double answer;

	while (1) {
		printf("Enter a number (or 'quit'): ");
		getaline(line);

		if(strcompare(line, "quit") == 0)
			break;

		if(check(line) == DIGITS){
			numbers[i++] = atoi(line);
			if(i == LIMIT) {
				printf("Array is full!\n");
				break;
			}
		} else {
			printf("%s is not all digits.\n", line);
		}
	}
	
	answer = average(numbers, i);
	printf("The average is: %.2f\n", answer);
}

//Functions
void getaline(char line[])
{
	int end;	
	fgets(line, MAX, stdin);
	line[strlen(line) - 1] = '\0';
}

int strcompare (char line[], char t[])
{
	int i = 0;

	/*Compare a char from each array until they are different or at end.*/

	while (line[i] == t[i]) {
		if (line[i++] == '\0') {
			return (0);
		}
	}
	return (line[i] - t[i]);
}

int check (char line[])
{
	int i = 0;

	// Checks for null character. And non-digits.

	for (; line[i] != '\0'; i++) {
		if (line[i] < '0' || line[i] > '9') {
			return(! DIGITS);
		}
	}
	return DIGITS;
}

int atoi (char line[])
{
	int i = 0, n = 0;

	while (line[i] >= '0' && line[i] <= '9') {
		n = 10 * n + line[i++] - '0';
	}

	return (n);
}

double average (int nums[], int amt)
{
	int i, sum = 0;

	for (i = 0; i < amt; i++) {
		sum += nums[i];
	}
	return((double) sum / amt);
}
