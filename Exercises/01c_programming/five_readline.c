/*five_readline.c*/
#include <stdio.h>

int main(void)
{
	char line[1024];
	int i = 0, c, k;

	while (( c = getchar()) != EOF) {
		line[i++ ] = c;

	line[i] = '\0';
	}

	//Printing the line
	for (k = 0; k < i; k++) {
		if (line[k] == '\n') {
			putchar('\n');
		}
		putchar(line[k]);
	}

}
