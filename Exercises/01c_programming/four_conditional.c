/*four_conditional.c*/
#include <stdio.h>

int main(void)
{
    #ifdef DEBUG
        printf("Debugging\n");
    #elif PRINT_ARRAY
        printf("Printing arrays\n");
    #endif
}
