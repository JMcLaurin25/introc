/*seven_circshift.c*/
#include <stdio.h>

unsigned int rcshift(unsigned int num, int size); //Right circular shift
unsigned int lcshift(unsigned int num, int size); //Left circular shift
int wordlength(void);

main()
{
	unsigned int n = 0777; // 111 111 111
	int num = 3;

	n = rcshift(n, num); //Shift the unsigned int 3 times to right
	printf("AFTER RIGHT: %o\n", n); //Outputs the octal number

	n = lcshift(n, num * num); //Shift the unsigned int 9 times to left
	printf("AFTER LEFT: %o\n", n);
}

/*Right Circular Shift*/
unsigned int rcshift(unsigned int number, int size)
{
	int i;
	unsigned mask = ~0;  //1111 1111 1111 1111

	// Shift existing mask to right, then 'flip' it.
	mask = ~(mask >> 1); //1000 0000 0000 0000 


	for (i = 0; i < size; i++) {
		if (number & 01) {//test for 'And'ing the number with 01
			number = (number >> 1) | mask; 
			//shift the number to right, 'or' with the mask
			// 0111 1111 1111 1111 | 1000 0000 0000 0000 = 1111 1111 1111 1111
		} else {
			number >>= 1; //If the number is not 01, then just shift it.
		}
	}
	return(number);
}

/*Left Circular Shift*/
unsigned int lcshift(unsigned int number, int size)
{
	unsigned int r;
	r = rcshift(number, wordlength() - size);
	return(r);
}

int wordlength(void)
{
	unsigned int x = ~0; //All 1's
	int count = 0;

	while (x != 0) {
		count++;
		x >>= 1;
	}
	return(count);
}
