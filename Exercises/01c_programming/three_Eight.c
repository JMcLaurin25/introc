/*three_Eight.c*/
#include <stdio.h>

main()
{
    int i = 1, upperLimit = 8;

    while (upperLimit > 0)
    {
        while (i < upperLimit)
        {
            printf("%d ", i);
            i++;
        };
        i = 1;
        upperLimit--;
        printf("\n");
    }
}
