/*five_copy.c*/
//Copy standard input to standard output
#include <stdio.h>

int main(void)
{
    int c;
    
    c = getchar();
    while ( c != -1) {
        putchar(c);
        c = getchar();
    }
}
