#include <stdio.h>

int strcompare (char line[], char t[])
{
	int i = 0;

	/*Compare a char from each array until they are different or at end.*/

	while (line[i] == t[i]) {
		if (line[i++] == '\0') {
			return (0);
		}
	}
	return (line[i] - t[i]);
}
