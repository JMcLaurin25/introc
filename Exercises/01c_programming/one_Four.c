/*one_Four.c*/
#include <stdio.h>

main()
{
    int a = 5, b = 10;
    double x = 25.5, y = 20;

    printf("Sum of %d and %d = %d\n", a, b, a + b);
    printf("Product of %d and %d = %d\n", a, b, a * b);
    printf("\nProduct of %f and %f = %f\n", x, y, x * y);
}
