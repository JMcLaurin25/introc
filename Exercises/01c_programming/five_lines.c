/*five_lines.c*/
#include <stdio.h>

//This counts the lines
int main(void)
{
	int lines = 0;
    int c;

	while(( c = getchar()) != EOF)
		if( c == '\n' )
			lines++;
	
	printf("%d lines\n", lines);

}
