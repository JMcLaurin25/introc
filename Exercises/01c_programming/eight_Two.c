#include <stdio.h>
#include <string.h>

#define MAX 100

void reverse(char []);
void changecase(char []);

int main(void)
{
	char input[MAX];

	fgets(input, MAX, stdin);
	input[strlen(input) - 1] = '\0';

	reverse(input);
	printf("Reversed: %s\n", input);

	changecase(input);
	printf("ChangeCase: %s\n", input);
	
	//printf("%d\n", 'a' - 'A');
	//printf("%c\n", 'A' + 32);

}

void reverse(char line[])
{
	char temp[MAX];
	int i, j = 0;
	
	i = strlen(line);
	temp[i + 1] = '\0';

	for (; i >= 1; i--) {
		temp[i-1] = line[j];
		j++;
	}

	for(i=0; i < strlen(line);i++)
		line[i] = temp[i];
		
}

void changecase(char line[])
{
	int i, inv;

	inv = 'a' - 'A';
	
	for (i = 0; i < strlen(line); i++) {
		if (line[i] >= 'a' && line[i] <= 'z')
			line[i] = line[i] - inv;
		else if (line[i] >= 'A' && line[i] <= 'Z')
			line[i] = line[i] + inv;
	}


}
