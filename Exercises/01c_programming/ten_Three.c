/*ten_Three.c*/
#include <stdio.h>
#include <string.h>

#define MAX 100

int array[MAX];

void put(int value, int position);
int get(int position);

int main(void)
{
	char input[MAX];
	int x, num1;
	register int i;

	for (i = 0; i < 5; i++) {
		printf("Enter a number: ");
		fgets(input, MAX, stdin);
		input[strlen(input + 1)] = '\0';

		num1 = atoi(input);

		put(num1, i);	//array[5] = 10;
	}

	while (i > 0) {
		i--;
		x = get(i); //x = array[5];
		printf("%d", x);
	}
	printf("\n");
}

void put(int value, int position)
{
	extern int array[MAX];
	array[position] = value;
}

int get(int position)
{
	extern int array[MAX];
	return (array[position]);
}
