/*six_Three.c

- Write a function that receives a string consisting of two fields and splits it into separate fields.
- Assume that the original string has the two fields separated by any number of blanks and tabs.

*/

#include <stdio.h>
#include <string.h>

#define MAX 100

void split(char [], char [], char[]);

int main(void)
{
	char string[MAX], field1[MAX], field2[MAX];

	fgets(string, MAX, stdin);
	split(string, field1, field2);
	printf("string1: %s\nstring2: %s\n", field1, field2);

}

void split(char strIn[], char strOut1[], char strOut2[])
{
	int i = 0, j = 0;

	while(strIn[i] == ' ' || strIn[i] == '\t')
		i++;

	while(strIn[i] != ' ' && strIn[i] != '\t')
		strOut1[j++] = strIn[i++];
	
	strOut1[j] = '\0';
	
	j = 0;	

	while(strIn[i] == ' ' || strIn[i] == '\t')
		i++;
	
	while(strIn[i] != ' ' && strIn[i] != '\t' && strIn[i] != '\0')
		strOut2[j++] = strIn[i++];

	strOut2[j] = '\0';

}
