/*eleven_swap.c*/
#include <stdio.h>

void swap(int *x, int *y);

int main(void)
{
	int a = 10, b = 5;

	printf("BEFORE: %d, %d\n", a, b);

	swap(&a, &b);

	printf("AFTER: %d, %d\n", a, b);

}

void swap(int *x, int *y)
{
	int temp;

	temp = *x;
	*x = *y;
	*y = temp;
}
