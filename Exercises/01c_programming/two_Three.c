/*two_Three.c*/
#include <stdio.h>

main()
{
    int a = 17, b = 4, c;

    c = a + b;
    printf("%d + %d = %d\n", a, b, c);

    c = a / b;
    printf("%d / %d = %d\n", a, b, c);

    c = a % b;
    printf("%d %s %d = %d\n", a, "%", b, c);
}
