#include <stdio.h>
#include <string.h>

#define MAX 100

void reverse(char []);

int main(void)
{
	char input[MAX];

	fgets(input, MAX, stdin);
	input[strlen(input) - 1] = '\0';

	reverse(input);

	printf("Reversed: %s\n", input);

}

void reverse(char line[])
{
	char temp[MAX];
	int i, j = 0;
	
	i = strlen(line);
	temp[i + 1] = '\0';

	for (; i >= 1; i--) {
		temp[i-1] = line[j];
		j++;
	}

	for(i=0; i < strlen(line);i++)
		line[i] = temp[i];
		
}
