/*three_Five.c*/
#include <stdio.h>

main()
{
    int i = 1, sum = 0;

    while ( i <= 100 )
    {
        sum += i;
        i++;
    }
    printf("\n\tThe sum of numbers between 1-100 is %d\n\n", sum);
}
